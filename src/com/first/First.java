package com.first;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;




class Example{
	public void mainCaller() {
		List<String> names =
		          Arrays.asList("Jon", "Ajeet", "Steve",
		             "Ajeet", "Jon", "Ajeet");

		      Map<String, Long> map =
		      names.stream().collect(
		          Collectors.groupingBy(
		             Function.identity(), Collectors.counting()
		          )
		      );

		      System.out.println(map);

	}
}

	class Student{    
		   int id;     
		   String name;  
		   int age;           
		   public Student(int id, String name, int age) {   
		       this.id = id;         
		       this.name = name;       
		       this.age = age;      
		   } 
		}  
	public class First {     
		   public static void main(String[] args) {       
		      List<Student> studentlist = new ArrayList<Student>();       
		      //Adding Students        
		      studentlist.add(new Student(11,"Jon",22));         
		      studentlist.add(new Student(22,"Steve",18));         
		      studentlist.add(new Student(33,"Lucy",22));         
		      studentlist.add(new Student(44,"Sansa",23));         
		      studentlist.add(new Student(55,"Maggie",18));                  
		      //Fetching student data as a Set       
		      Set<Student> students = studentlist.stream()
		                           .filter(n-> n.id>22)
		                           .collect(Collectors.toSet());
		      //Iterating Set       
		      for(Student stu : students) { 
		         System.out.println(stu.id+" "+stu.name+" "+stu.age); 
		      } 
		      
		      
		   } 
		}
