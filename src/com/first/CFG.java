package com.first;

import java.util.*;

class GFG {

	public static void main(String[] args) {

		String[] array = { "Geeks", "for", "Geeks" };

		Optional<String> String_combine = Arrays.stream(array).reduce((str1, str2) -> str1 + "-" + str2);

		if (String_combine.isPresent()) {
			System.out.println(String_combine.get());
		}
	}
}
