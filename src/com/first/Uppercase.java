package com.first;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Uppercase {
	
	public static void main(String[] arg) {
		Stream<String> names = Stream.of("aBc", "d", "ef");
		System.out.println(names.map(s -> {
				return s.toUpperCase();
			}).collect(Collectors.toList()));
	}

}
