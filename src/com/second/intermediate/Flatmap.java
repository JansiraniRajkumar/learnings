package com.second.intermediate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Flatmap {
	
	
	public static void main(String[] args) {
	Stream<List<String>> namesOriginalList = Stream.of(
			Arrays.asList("Pankaj"), 
			Arrays.asList("David", "Lisa"),
			Arrays.asList("Amit"));
		Stream<String> flatStream = namesOriginalList
			.flatMap(strList -> strList.stream());

		flatStream.forEach(System.out::println);
	}

}
